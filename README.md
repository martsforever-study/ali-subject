# 说明

在工程根目录下需要先安装npm依赖

```
npm install
```

安装完毕之后，请打开我的有道云笔记，里面写明了操作指引。因为有道云笔记有markdown图床，方便我展示第四道题的思路。谢谢
[笔试题.md](http://note.youdao.com/noteshare?id=f0111a1e94fd7c6eb5b25c3acb8e6418&sub=B4A5D873096B4515A8D4913A2D5F7300)
