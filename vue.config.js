const resolve = require('path').resolve

const isProd = process.env.NODE_ENV === 'production'
console.log('is production:' + process.env.NODE_ENV === 'production')

const option = {
    publicPath: './',
    productionSourceMap: true,
    devServer: {
        port: '7554',
    },
    pages: {
        index: {
            // page 的入口
            entry: 'subject3/demo/main.js',
            // 模板来源
            template: 'public/index.html',
            // 在 dist/index.html 的输出
            filename: 'index.html',
            // 当使用 title 选项时，
            // template 中的 title 标签需要是 <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'ali-subject',
            // 在这个页面中包含的块，默认情况下会包含
            // 提取出来的通用 chunk 和 vendor chunk。
            chunks: ['chunk-vendors', 'chunk-common', 'index']
        },
    },
    configureWebpack: {
        externals: {
            ...(isProd ? {
                'vue': 'Vue'
            } : {})
        },
        resolve: {
            extensions: ['.js', '.vue', '.json'],
            alias: {
                'src': resolve('./subject3/src'),
                'demo': resolve('./subject3/demo'),
                'subject': resolve('./subject3/src'),
            }
        },

    },
    css: {
        loaderOptions: {
            sass: {
                data: `@import "src/styles/global.scss"; @import "demo/global.scss";`
            }
        }
    },
    chainWebpack: config => {
        config.plugins.delete('prefetch-index')
    }
}

module.exports = option
