import './styles/index.scss'

import SearchInput from './components/search-input'

import Lang from './lang'

export {
    SearchInput,

    Lang,
}

