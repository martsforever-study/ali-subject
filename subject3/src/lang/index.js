import zhCn from './language/zh-cn'

/**
 * 多语言插件
 * @author  韦胜健
 * @date    2019/7/26 15:18
 */
export default {
    Vue: null,
    instance: null,

    install(Vue) {
        this.Vue = Vue
        this.instance = new this.Vue({
            data() {
                return {
                    lang: {},
                }
            },
            render(h) {
                return h('div')
            },
        })
        this.Vue.prototype.$t = this.instance.lang
        this.change(zhCn)
    },
    change(data) {
        Object.keys(this.instance.lang).forEach(key => {
            this.instance.lang[key] = null
        })
        Object.keys(data).forEach(key => {
            this.instance.$set(this.instance.lang, key, data[key])
        })
    },
}

