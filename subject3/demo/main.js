import Vue from 'vue'
import App from './App'
import './index.scss'
import {SearchInput, Lang} from 'subject'

Vue.component(SearchInput.name, SearchInput)

Vue.use(Lang)

new Vue({
    render: h => h(App),
}).$mount('#app')
