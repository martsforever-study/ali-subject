const inquirer = require('inquirer');

/**
 * 问题描述：给定一个指定数组，类似于 [1,3,4,9,19] ，数组数量不定，找出最接近平均数的数字。 20%
 * @author  韦胜健
 * @date    2019/7/23 18:51
 */
const app = {
    /**
     * 参数获取选项
     * @author  韦胜健
     * @date    2019/7/23 20:41
     */
    promptOptions: {
        cancel: {
            order: 5,
            tip: '取消',
            handler() {
            },
        },
        subject: {
            order: 1,
            tip: '题目示例数据：[1,3,4,9,19]',
            async handler() {
                const array = [1, 3, 4, 9, 19];
                this.startCalc(array);
            },
        },
        manual: {
            order: 2,
            tip: '手动输入一个数组数据',
            async handler() {
                let {array} = await inquirer.prompt({
                    type: 'input',
                    message: '手动输入测试数组（示例：[ 1.43, 2.32, 2.87, 3.15, 3.64, 6.26, 6.78, 9.04, 9.08, 9.48 ]）',
                    name: 'array',
                    validate: (string) => {
                        try {
                            const array = JSON.parse(string);
                            if (!Array.isArray(array)) {
                                console.error('数组格式不正确，请检查！');
                                return false;
                            } else {
                                return true
                            }
                        } catch (e) {
                            console.error('数组格式不正确，请检查！');
                            return false;
                        }
                    },
                });
                array = JSON.parse(array);
                this.startCalc(array);
            },
        },
        file: {
            order: 3,
            tip: '在编辑器中输入多组数组',
            async handler() {
                console.log('可以在编辑器中输入多组数据，示例');
                console.log('[ 0.62, 1.38, 1.59, 1.99, 2.61, 2.72, 4.75, 5.28, 5.7, 9.61 ]');
                console.log('[ 0.57, 2.72, 3.35, 3.63, 4.09, 4.71, 6.99, 7.93, 9.87, 9.88 ]');
                console.log('[ 0.57, 2.72, 3.35, 3.63, 4.09, 4.71, 6.99, 7.93, 9.87, 9.88 ]');
                console.log('[ 0.04, 0.88, 2.31, 3.21, 3.43, 6.78, 6.84, 7.36, 7.77, 7.95 ]');
                console.log('[ 1.67, 1.95, 2.16, 4.43, 5.99, 6.19, 7.24, 8.93, 9.35, 9.42 ]');

                let arrayList;

                await inquirer.prompt({
                    type: 'editor',
                    message: '在编辑器中输入多组数组。回车打开编辑器，在编辑器中保存，关闭继续下一步操作：',
                    name: 'arrayList',
                    validate: (string) => {
                        string = string.trim();
                        if (!string) {
                            console.error('输入内容为空！');
                            return false
                        }
                        try {
                            arrayList = string.split('\n').map(str => {
                                str = str.trim();
                                const arr = JSON.parse(str);
                                if (!Array.isArray(arr)) {
                                    throw new Error(arr)
                                }
                                return arr
                            });
                            return true
                        } catch (e) {
                            console.error('数组格式不正确：' + e.toString());
                            return false
                        }
                    },
                });

                arrayList.forEach(arr => this.startCalc(arr))
            },
        },
        auto: {
            order: 4,
            tip: '自动生成十个个随机数组',
            async handler() {
                const validate = (num) => {
                    num = num - 0;
                    if (!isNaN(num)) return true;
                    return '数字不正确！请摁上下键去掉NaN';
                };
                const {arrayLength} = await inquirer.prompt({type: 'number', message: '请输入随机数组长度', name: 'arrayLength', default: 10, validate});
                const {maxNumber} = await inquirer.prompt({type: 'number', message: '请输入随机数组最大值', name: 'maxNumber', default: 10, validate});
                console.log(`数组长度：${arrayLength},最大值：${maxNumber}`);
                for (let i = 0; i < 10; i++) {
                    const randomArray = [];
                    for (let i = 0; i < arrayLength; i++) {
                        randomArray.push((Math.random() * maxNumber).toFixed(2) - 0)
                    }
                    this.startCalc(randomArray);
                }
            },
        },
    },
    /**
     * 计算数组中最接近平均值的数字
     * @author  韦胜健
     * @date    2019/7/23 20:36
     * @param   array   数组参数
     */
    startCalc(array) {
        if (!Array.isArray(array)) {
            console.error('数组格式不正确！');
            return;
        }
        if (array.length === 0) {
            console.error('数组长度为0！');
            return;
        }
        array = [...array];
        //均值
        let avg = 0;
        //余数
        let res = 0;
        //数组长度
        let len = array.length;
        array.forEach(num => {
            avg += Math.floor(num / len);
            res += num % len;
            avg += Math.floor(res / len);
            res = res % len
        });
        avg = (avg + (res / len)).toFixed(5) - 0;
        let minAbs = Infinity;
        let closeNumbers = [];
        array.forEach(item => {
            const abs = Math.abs(item - avg);
            if (abs === minAbs) {
                closeNumbers.push(item);
            } else if (abs < minAbs) {
                minAbs = abs;
                closeNumbers = [item]
            }
        });
        const ret = Array.from(new Set(closeNumbers)).sort((a, b) => a - b);
        console.log('数组：' + array.sort((a, b) => a - b));
        console.log('均值：' + avg);
        console.log(`接近均值的数字：${ret}\n`);
        return ret
    },
    /**
     * 启动命令行程序
     * @author  韦胜健
     * @date    2019/7/23 20:39
     */
    async start() {
        const option = {
            type: 'rawlist',
            message: '请选择输入测试参数的方式',
            name: 'inputType',
            choices: Object.keys(this.promptOptions)
                .sort((a, b) => this.promptOptions[a].order - this.promptOptions[b].order)
                .map(key => ({name: this.promptOptions[key].tip, value: key,}))
        };
        const {inputType} = await inquirer.prompt(option);
        this.promptOptions[inputType].handler.call(app);
    }
};

app.start();



