const inquirer = require('inquirer');
var ejs = require('ejs');

const fs = require('fs');
const path = require('path');
const join = path.join;
const resolve = path.resolve;

const app = {
    /**
     * 判断文件是否存在
     * @author  韦胜健
     * @date    2019/7/24 21:47
     */
    fsExistsSync(dir) {
        try {
            fs.accessSync(dir);
            return true;
        } catch (e) {
            return false;
        }
    },
    /**
     * 排除扫描的文件
     * @author  韦胜健
     * @date    2019/7/24 21:52
     */
    excludeFile: [
        'node_modules',
        'dist',
        '.idea',
        '.git',
    ],
    /**
     * 扫描目录文件信息
     * @author  韦胜健
     * @date    2019/7/24 21:52
     */
    collectDirCascadeData(baseDir) {
        let files = fs.readdirSync(baseDir);
        return files.reduce((ret, item, index) => {
            if (this.excludeFile.indexOf(item) > -1) return ret;
            let fPath = join(baseDir, item);
            let isDir = fs.statSync(fPath).isDirectory();
            ret.push({
                name: item,
                path: fPath,
                isDir,
                index,
                children: !isDir ? null : this.collectDirCascadeData(fPath),
            });
            return ret;
        }, []).sort((a, b) => {
            if (a.isDir && !b.isDir) return -1;
            else if (!a.isDir && b.isDir) return 1;
            else return a.index - b.index
        });
    },
    async start() {
        let {rootDir} = await inquirer.prompt({type: 'input', message: `请指定扫描的目录：(默认排除：${this.excludeFile.join(',')})`, suffix: '（Windows示例：D:/4_keep，MacOS示例：/web）', name: 'rootDir', default: './'});
        rootDir = resolve(rootDir);
        console.log('扫描目录：', rootDir);
        if (!this.fsExistsSync(rootDir)) {
            console.error('目录不存在！');
            return
        }
        const dirCascadeData = this.collectDirCascadeData(rootDir);
        // dirCascadeData.forEach(item => console.dir(item));

        const ejsTemplate = fs.readFileSync(resolve('./subject1/index.ejs'), 'utf-8');
        const htmlString = ejs.render(ejsTemplate, {
            title: rootDir,
            root: {
                name: rootDir,
                path: rootDir,
                isDir: true,
                index: 0,
                children: dirCascadeData,
            }
        });
        fs.writeFileSync(resolve('./subject1/index.html'), htmlString);
    },
};

app.start();
